import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from './app.service';
import { GroupsModule } from '../services/groups/groups.module';
import { CoursesModule } from '../services/courses/courses.module';
import { StudentsModule } from '../services/students/students.module';
import { CoursesControllerModule } from '../controllers/courses/courses.controller.module';
import { GroupsControllerModule } from '../controllers/groups/groups.controller.module';
import { StudentsControllerModule } from '../controllers/students/students.controller.module';
import { typeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { ConfigModule } from '../configs/config.module';
import { AuthControllerModule } from '../controllers/auth/auth.controller.module';
import { AuthModule } from '../services/auth/auth.module';
import { ResetTokenModule } from '../services/reset-token/reset-token.module';
import { LectorsModule } from '../services/lectors/lectors.module';
import { LectorsControllerModule } from '../controllers/lectors/lectors.controller.module';
import { MarksControllerModule } from '../controllers/marks/marks.controller.module';
import { MarksModule } from '../services/marks/marks.module';
import { MailModule } from '../services/mail/mail.module';
import { MulterModule } from '@nestjs/platform-express';
import { UsersControllerModule } from 'src/controllers/users/users.controller.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    ConfigModule,
    GroupsModule,
    CoursesModule,
    LectorsModule,
    StudentsModule,
    MarksModule,
    LectorsControllerModule,
    CoursesControllerModule,
    GroupsControllerModule,
    StudentsControllerModule,
    MarksControllerModule,
    AuthControllerModule,
    AuthModule,
    ResetTokenModule,
    MailModule,
    UsersControllerModule,
    MulterModule.register({
      dest: './uploads',
    }),
  ],
  providers: [AppService],
})
export class AppModule {}
