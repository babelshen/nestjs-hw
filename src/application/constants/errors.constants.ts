export const Unauthorized = {
  message: 'Unauthorized',
  statusCode: 401,
};

export const InternalServerError = {
  statusCode: 500,
  message: 'Internal Server Error',
};

export const NonCorrectId = {
  message: 'Validation failed (uuid is expected)',
  error: 'Bad Request',
  statusCode: 400,
};
