import { ApiProperty } from '@nestjs/swagger';
import {
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

export abstract class CoreEntity extends BaseEntity {
  @ApiProperty({
    example: 'b0e31418-e591-43c5-bbe5-de4990a519d7',
    type: String,
    description: 'UUID of the student',
  })
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Creation date and time of the student',
  })
  @CreateDateColumn({ type: 'timestamp with time zone', name: 'created_at' })
  public createdAt: Date;

  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Last update date and time of the student',
  })
  @UpdateDateColumn({ type: 'timestamp with time zone', name: 'updated_at' })
  public updatedAt: Date;
}
