import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class QueryFilterDto {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    example: 'name',
    type: String,
    description: 'Sort by field',
  })
  public sortField?: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    example: 'DESC',
    type: String,
    description: 'Order by field',
  })
  public sortOrder?: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    example: 'Admin',
    type: String,
    description: 'Search by name',
  })
  public name?: string;
}
