import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignUpRequestDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Albus Dumbledore',
    type: String,
    description: 'The name of the user',
  })
  username: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'albusdumbledore@gmail.com',
    type: String,
    description: 'The email of the user',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '123d345',
    type: String,
    description: 'The password of the user',
  })
  password: string;
}
