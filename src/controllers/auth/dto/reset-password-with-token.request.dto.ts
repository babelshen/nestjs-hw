import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordWithTokenRequestDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'garrypotter@gmail.com',
    type: String,
    description: 'The email of the user',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'baf843f4e3a6934f4f04849113323e8eb47625a2a5bcf31dcdb0ce08faf97ae0',
    type: String,
    description: 'Token',
  })
  token: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'garry',
    type: String,
    description: 'The old password of the user',
  })
  oldPassword: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'garry123',
    type: String,
    description: 'New password of the user',
  })
  newPassword: string;
}
