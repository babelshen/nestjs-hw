import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignResponseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIwNjAwMTNmMi1hOTViLTRjYmQtYTEwYy02ZTBkN..._7q6kmRk',
    type: String,
    description: 'The access token',
  })
  accessToken: string;
}
