import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordRequestDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'garrypotter@gmail.com',
    type: String,
    description: 'The email of the user',
  })
  email: string;
}
