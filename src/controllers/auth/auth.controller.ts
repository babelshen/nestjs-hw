import { Body, Controller, Post, HttpCode, HttpStatus } from '@nestjs/common';
import { AuthService } from '../../services/auth/auth.service';
import { SignUpRequestDto } from './dto/sign-up.request.dto';
import { SignResponseDto } from './dto/sign.response.dto';
import { ResetPasswordRequestDto } from './dto/reset-password.request.dto';
import { ResetPasswordWithTokenRequestDto } from './dto/reset-password-with-token.request.dto';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ResetPasswordResponseDto } from './dto/reset-password.response.dto';
import { SignInRequestDto } from './dto/sign-in.request.dto';
import { InternalServerError } from '../../application/constants/errors.constants';
import { GetAccessTokenDto } from '../../services/reset-token/dto/get-access-token.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: 'Sign in' })
  @ApiBody({
    type: SignInRequestDto,
    description: 'Sign in form',
    examples: {
      based: {
        summary: 'Example of sign in form',
        value: {
          email: 'garrypotter@gmail.com',
          password: 'garry',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: SignResponseDto,
    description: 'Access token',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation error',
    schema: {
      example: {
        message: ['email must be an email'],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized exception',
    schema: {
      example: {
        message: 'Incorrect email or password',
        error: 'Unauthorized',
        statusCode: 401,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Account not found',
    schema: {
      example: {
        message: 'You entered an incorrect email',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
    schema: {
      example: InternalServerError,
    },
  })
  @HttpCode(HttpStatus.OK)
  @Post('sign-in')
  public signIn(@Body() signInDto: SignInRequestDto): Promise<SignResponseDto> {
    return this.authService.signIn(signInDto.email, signInDto.password);
  }

  @ApiOperation({ summary: 'Sign up' })
  @ApiBody({
    type: SignUpRequestDto,
    description: 'Sign up form',
    examples: {
      based: {
        summary: 'Example of sign up form',
        value: {
          username: 'Garry Potter',
          email: 'garrypotter@gmail.com',
          password: 'garry',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: SignResponseDto,
    description: 'Access token',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Dublicate e-mail | Validation failed',
    schema: {
      example: {
        message: 'Lector with this e-mail already exists',
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
    schema: {
      example: InternalServerError,
    },
  })
  @HttpCode(HttpStatus.OK)
  @Post('sign-up')
  public signUp(@Body() signUpDto: SignUpRequestDto): Promise<SignResponseDto> {
    return this.authService.signUp(signUpDto);
  }
  @ApiOperation({ summary: 'Reset password request' })
  @ApiBody({
    type: ResetPasswordRequestDto,
    description: 'Reset password request form',
    examples: {
      based: {
        summary: 'Example of reset password request form',
        value: {
          email: 'garrypotter@gmail.com',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    type: GetAccessTokenDto,
    description: 'Reset token',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation error',
    schema: {
      example: {
        message: ['email must be an email'],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Account not found',
    schema: {
      example: {
        message: 'You entered an incorrect email',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
    schema: {
      example: InternalServerError,
    },
  })
  @HttpCode(HttpStatus.OK)
  @Post('reset-password-request')
  public resetPasswordRequest(
    @Body() resetPasswordDto: ResetPasswordRequestDto,
  ): Promise<ResetPasswordResponseDto> {
    return this.authService.resetPasswordRequest(resetPasswordDto.email);
  }

  @ApiOperation({ summary: 'Reset password' })
  @ApiBody({
    type: ResetPasswordWithTokenRequestDto,
    description: 'Reset password form',
    examples: {
      based: {
        summary: 'Example of reset password request form',
        value: {
          email: 'garrypotter@gmail.com',
          token:
            '0f0cc9b0ca2eba6ca62190d7c623e490643546c77cb30130ef15664ccf2a899',
          oldPassword: 'garry',
          newPassword: 'garry123',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Reset password',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation error | User not found | Non-correct old password',
    schema: {
      example: {
        message: ['email must be an email'],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Not found email',
    schema: {
      example: {
        message: 'You entered an incorrect email',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Something went wrong',
    schema: {
      example: InternalServerError,
    },
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('reset-password')
  public resetPassword(
    @Body() resetPasswordDto: ResetPasswordWithTokenRequestDto,
  ): Promise<void> {
    return this.authService.resetPassword(resetPasswordDto);
  }
}
