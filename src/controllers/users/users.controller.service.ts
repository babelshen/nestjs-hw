import { Injectable, NotFoundException } from '@nestjs/common';
import { LectorsService } from 'src/services/lectors/lectors.service';
import { UserResponseDto } from './dto/user-response.dto';

@Injectable()
export class UsersControllerService {
  constructor(private readonly lectorsService: LectorsService) {}

  public async findCurrentUser(userId: string): Promise<UserResponseDto> {
    const user = await this.lectorsService.findOneLector(userId);

    if (!user) {
      throw new NotFoundException(`User is not found`);
    }

    return { userId: user.id, email: user.email };
  }
}
