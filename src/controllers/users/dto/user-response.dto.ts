import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserResponseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'c134709d-2ebf-4eda-8043-85c4e55314b8',
    type: String,
    description: 'The id of the user',
  })
  userId: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'garrypotter@email.com',
    type: String,
    description: 'The email of the user',
  })
  email: string;
}
