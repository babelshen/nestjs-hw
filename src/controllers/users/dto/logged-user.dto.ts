import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LoggedUserDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'c134709d-2ebf-4eda-8043-85c4e55314b8',
    type: String,
    description: 'The id of the user',
  })
  userId: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Garry',
    type: String,
    description: 'The nickname of the user',
  })
  username: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    example: 1694280264,
    type: Number,
    description: 'Issued at',
  })
  iat: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    example: 1694316264,
    type: Number,
    description: 'Expiration time',
  })
  exp: number;
}
