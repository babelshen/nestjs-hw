import { Controller, Get, HttpStatus, UseGuards } from '@nestjs/common';
import { UsersControllerService } from './users.controller.service';
import { CurrentUser } from '../../services/auth/decorators/current.user.decorator';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UserResponseDto } from './dto/user-response.dto';
import { LoggedUserDto } from './dto/logged-user.dto';
import {
  InternalServerError,
  Unauthorized,
} from '../../application/constants/errors.constants';

@ApiTags('User')
@UseGuards(AuthGuard)
@ApiBearerAuth()
@Controller('')
@ApiBearerAuth('jwt')
export class UsersController {
  constructor(
    private readonly usersControllerService: UsersControllerService,
  ) {}

  @ApiOperation({ summary: 'Return user-lector from form login' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get user-lector from form login successfully',
    type: UserResponseDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
    schema: {
      example: {
        message: 'Validation failed',
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'User is not found',
    schema: {
      example: {
        message: 'User is not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Get('me')
  public async findMe(
    @CurrentUser()
    loggedUser: LoggedUserDto,
  ): Promise<UserResponseDto> {
    console.log(loggedUser);
    return await this.usersControllerService.findCurrentUser(loggedUser.userId);
  }
}
