import { Injectable } from '@nestjs/common';
import { LectorsService } from '../../services/lectors/lectors.service';
import { CreateLectorDto } from '../../services/lectors/dto/create-lector.dto';
import { QueryFilterDto } from '../../application/dto/query.filter.dto';
import { UpdateLectorDto } from 'src/services/lectors/dto/update-lector.dto';

@Injectable()
export class LectorsControllerService {
  constructor(private readonly lectorsService: LectorsService) {}
  createLector(createLectorDto: CreateLectorDto) {
    return this.lectorsService.createLector(createLectorDto);
  }

  findAllLectors(queryFilter?: QueryFilterDto) {
    return this.lectorsService.findAllLectors(queryFilter);
  }

  findOneLector(id: string) {
    return this.lectorsService.findOneLector(id);
  }

  addCourseForLector(lectorId: string, courseId: string) {
    return this.lectorsService.addCourseForLector(lectorId, courseId);
  }

  getCourseOfLector(id: string) {
    return this.lectorsService.getCourseOfLector(id);
  }

  updateLector(id: string, updateLectorDto: UpdateLectorDto) {
    return this.lectorsService.updateLector(id, updateLectorDto);
  }
}
