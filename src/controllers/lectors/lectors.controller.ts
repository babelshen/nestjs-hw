import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Patch,
  ParseUUIDPipe,
  UseGuards,
  HttpStatus,
  Query,
} from '@nestjs/common';
import { LectorsControllerService } from './lectors.controller.service';
import { CreateLectorDto } from '../../services/lectors/dto/create-lector.dto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetLectorDto } from '../../services/lectors/dto/get-lector.response.dto';
import { GetLectorWithCourseDto } from '../../services/lectors/dto/get-lector-with-course.response.dto';
import { GetCourseOfLectorDto } from '../../services/lectors/dto/get-course-of-lector.response.dto';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import {
  InternalServerError,
  NonCorrectId,
  Unauthorized,
} from '../../application/constants/errors.constants';
import { QueryFilterDto } from '../../application/dto/query.filter.dto';
import { UpdateLectorDto } from '../../services/lectors/dto/update-lector.dto';

@ApiTags('Lectors')
@UseGuards(AuthGuard)
@Controller('lectors')
@ApiBearerAuth('jwt')
export class LectorsController {
  constructor(
    private readonly lectorsControllerService: LectorsControllerService,
  ) {}

  @ApiOperation({ summary: 'Create new lector' })
  @ApiBody({
    type: CreateLectorDto,
    description: 'Create lector request',
    examples: {
      based: {
        summary: 'Example of create Lector',
        value: {
          name: 'Albus Dumbledore',
          email: 'albusdumbledore@gmail.com',
          password: '12341',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create new lector',
    type: GetLectorDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Dublicate e-mail | Validation failed',
    schema: {
      example: {
        message: 'Lector with this e-mail already exists',
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Post('/')
  createLector(
    @Body() createLectorDto: CreateLectorDto,
  ): Promise<GetLectorDto> {
    return this.lectorsControllerService.createLector(createLectorDto);
  }

  @ApiOperation({ summary: 'Get all lectors' })
  @ApiQuery({
    name: 'name',
    required: false,
    type: String,
    description: 'Name of the lector',
    example: 'Albus',
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: String,
    description: 'Order by field',
    example: 'DESC',
  })
  @ApiQuery({
    name: 'sortField',
    required: false,
    type: String,
    description: 'Sort by field',
    example: 'name',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List of lectors',
    isArray: true,
    type: GetLectorDto,
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Get('/')
  findAllLector(
    @Query() queryFilter?: QueryFilterDto,
  ): Promise<GetLectorDto[]> {
    return this.lectorsControllerService.findAllLectors(queryFilter);
  }

  @ApiOperation({ summary: 'Get lector by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Lector by id',
    type: GetLectorWithCourseDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid Lector id',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector not found',
    schema: {
      example: {
        message: 'Lector not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the lector' })
  @Get(':id')
  findOneLector(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<GetLectorWithCourseDto> {
    return this.lectorsControllerService.findOneLector(id);
  }

  @ApiOperation({ summary: 'Add course for lector' })
  @ApiBody({
    description: 'Request adding course for lector request',
    examples: {
      based: {
        summary: 'Example of request',
        value: {
          courseId: '88087287-b2c0-4d39-ad5d-107cd9fc6cf3',
        },
      },
    },
  })
  @ApiParam({
    name: 'lectorId',
    type: String,
    description: 'UUID of the lector',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Add course for lector',
    type: GetLectorWithCourseDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid lector id | Validation failed',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector not found | Course not found',
    schema: {
      example: {
        message: 'Lector not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Patch(':lectorId/course')
  addCourseForLector(
    @Param('lectorId', ParseUUIDPipe) lectorId: string,
    @Body() body: { courseId: string },
  ): Promise<GetLectorWithCourseDto> {
    return this.lectorsControllerService.addCourseForLector(
      lectorId,
      body.courseId,
    );
  }
  @ApiOperation({ summary: 'Get courses of lector' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get courses of lector',
    isArray: true,
    type: GetCourseOfLectorDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid Lector id',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector not found',
    schema: {
      example: {
        message: 'Lector not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the lector' })
  @Get(':id/courses')
  getCourseOfLector(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<GetCourseOfLectorDto[]> {
    return this.lectorsControllerService.getCourseOfLector(id);
  }

  @ApiOperation({ summary: 'Update lector by id' })
  @ApiBody({
    type: UpdateLectorDto,
    description: 'Update lector request',
    examples: {
      based: {
        summary: 'Example of update Lector',
        value: {
          name: 'Albus Dumbledore',
          email: 'albusdumbledore@gmail.com',
          password: '12341',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Update lector by id',
    type: GetLectorDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed | Invalid lector id',
    schema: {
      example: {
        message: [
          'The name should not be more than fifty characters',
          'The name must contain a minimum of three characters',
          'name must be a string',
        ],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector not found',
    schema: {
      example: {
        message: 'Lector not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the lector' })
  @Patch(':id')
  updateLector(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateLectorDto: UpdateLectorDto,
  ): Promise<GetLectorDto> {
    return this.lectorsControllerService.updateLector(id, updateLectorDto);
  }
}
