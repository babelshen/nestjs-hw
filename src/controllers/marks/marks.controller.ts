import { Body, Controller, HttpStatus, Post, UseGuards } from '@nestjs/common';
import { MarksControllerService } from './marks.controller.service';
import { AddMarktDto } from '../../services/marks/dto/add-mark-for-student.dto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import {
  InternalServerError,
  Unauthorized,
} from '../../application/constants/errors.constants';

@ApiTags('Marks')
@UseGuards(AuthGuard)
@Controller('marks')
@ApiBearerAuth('jwt')
export class MarksController {
  constructor(
    private readonly marksControllerService: MarksControllerService,
  ) {}

  @ApiOperation({ summary: 'Add mark for student' })
  @ApiBody({
    type: AddMarktDto,
    description: 'Add mark for student',
    examples: {
      based: {
        summary: 'Example of adding mark',
        value: {
          mark: 10,
          lector_id: '45ee8032-16fe-414e-a164-37fd76997f0f',
          course_id: '88087287-b2c0-4d39-ad5d-107cd9fc6cf3',
          student_id: 'b0e31418-e591-43c5-bbe5-de4990a519d7',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Adding mark for student',
    type: AddMarktDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed',
    schema: {
      example: {
        message: [
          'lector_id must be a UUID',
          'course_id must be a UUID',
          'student_id must be a UUID',
        ],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Lector not found | Course not found | Student not found',
    schema: {
      example: {
        message: 'Lector not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Post('/')
  createStudent(@Body() addMartDto: AddMarktDto): Promise<AddMarktDto> {
    return this.marksControllerService.addMark(addMartDto);
  }
}
