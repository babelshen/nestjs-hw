import { Injectable } from '@nestjs/common';
import { MarksService } from '../../services/marks/marks.service';
import { AddMarktDto } from 'src/services/marks/dto/add-mark-for-student.dto';

@Injectable()
export class MarksControllerService {
  constructor(private readonly marksService: MarksService) {}

  addMark(addMarkDto: AddMarktDto) {
    return this.marksService.addMark(addMarkDto);
  }
}
