import { Injectable } from '@nestjs/common';
import { StudentsService } from '../../services/students/students.service';
import { CreateStudentDto } from '../../services/students/dto/create-student.dto';
import { UpdateStudentDto } from '../../services/students/dto/update-student.dto';
import { AddGroupDto } from 'src/services/students/dto/add-group-for-student.dto';
import { AddCourseDto } from 'src/services/students/dto/add-course-for-student.dto';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class StudentsControllerService {
  constructor(private readonly studentsService: StudentsService) {}
  createStudent(createStudentDto: CreateStudentDto) {
    return this.studentsService.createStudent(createStudentDto);
  }

  findAllStudents(queryFilter?: QueryFilterDto) {
    return this.studentsService.findAllStudents(queryFilter);
  }

  findOneStudent(id: string) {
    return this.studentsService.findOneStudent(id);
  }

  updateStudent(id: string, updateStudentDto: UpdateStudentDto) {
    return this.studentsService.updateStudent(id, updateStudentDto);
  }

  removeStudent(id: string) {
    return this.studentsService.removeStudent(id);
  }

  addGroupForStudent(id: string, groupId: AddGroupDto) {
    return this.studentsService.addGroupForStudent(id, groupId);
  }

  addCourseForStudent(id: string, courseIdDto: AddCourseDto) {
    return this.studentsService.addCourseForStudent(id, courseIdDto);
  }

  getStudentMarks(id: string) {
    return this.studentsService.getStudentMarks(id);
  }

  addImage(id: string, file?: string) {
    return this.studentsService.addImage(id, file);
  }
}
