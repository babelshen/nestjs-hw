import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  ParseUUIDPipe,
  HttpStatus,
  UseInterceptors,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { StudentsControllerService } from './students.controller.service';
import { CreateStudentDto } from '../../services/students/dto/create-student.dto';
import { UpdateStudentDto } from '../../services/students/dto/update-student.dto';
import { AddGroupDto } from '../../services/students/dto/add-group-for-student.dto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetStudentDto } from '../../services/students/dto/get-students-response.dto';
import { GetStudentMarkDto } from '../../services/students/dto/get-student-marks-response.dto';
import { GetStudentWithGroupDto } from '../../services/students/dto/get-student-with-group.response.dto';
import {
  InternalServerError,
  NonCorrectId,
  Unauthorized,
} from '../../application/constants/errors.constants';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import {
  editFileName,
  imageFileFilter,
} from 'src/application/middleware/upload.middleware';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import { AddCourseDto } from '../../services/students/dto/add-course-for-student.dto';
import { GetStudentWithCourseDto } from '../../services/students/dto/get-student-with-course.dto';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@ApiTags('Students')
@UseGuards(AuthGuard)
@Controller('students')
@ApiBearerAuth('jwt')
export class StudentsController {
  constructor(
    private readonly studentsControllerService: StudentsControllerService,
  ) {}

  @ApiOperation({ summary: 'Create new student' })
  @ApiBody({
    type: CreateStudentDto,
    description: 'Create student request',
    examples: {
      based: {
        summary: 'Example of create student',
        value: {
          name: 'Garry',
          surname: 'Potter',
          email: 'example@mail.com',
          age: 17,
          groupId: '2090f28f-0a2f-4642-8851-7237ae2c57d1',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create new student',
    type: GetStudentDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Dublicate e-mail | Validation failed',
    schema: {
      example: {
        message: 'Student with this email already exists',
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Post()
  createStudent(
    @Body() createStudentDto: CreateStudentDto,
  ): Promise<GetStudentDto> {
    return this.studentsControllerService.createStudent(createStudentDto);
  }

  @ApiOperation({
    summary: 'Get all students or student with a specific name',
  })
  @ApiQuery({
    name: 'name',
    required: false,
    type: String,
    description: 'Name of the student',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List of students or student with a specific name',
    type: GetStudentDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Get()
  findAllStudents(
    @Query() queryFilter?: QueryFilterDto,
  ): Promise<GetStudentDto[]> {
    return this.studentsControllerService.findAllStudents(queryFilter);
  }

  @ApiOperation({ summary: 'Get student by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Student by id',
    type: GetStudentDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid student id',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found',
    schema: {
      example: {
        message: 'Student not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the student' })
  @Get(':id')
  findOneStudent(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<GetStudentWithGroupDto> {
    return this.studentsControllerService.findOneStudent(id);
  }

  @ApiOperation({ summary: 'Update student by id' })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the student' })
  @ApiBody({
    type: UpdateStudentDto,
    description: 'Request for updating params of student data',
    examples: {
      one_field: {
        summary: 'Example of updating one field of student',
        value: {
          age: 19,
        },
      },
      all_fields: {
        summary: 'Update all fields',
        value: {
          name: 'Garry',
          surname: 'Potter',
          email: 'example@mail.com',
          age: 17,
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Update student by id',
    type: GetStudentDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid student id | Validation failed',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found',
    schema: {
      example: {
        message: 'Student not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Patch(':id')
  updateStudent(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateStudentDto: UpdateStudentDto,
  ): Promise<GetStudentDto> {
    return this.studentsControllerService.updateStudent(id, updateStudentDto);
  }

  @ApiOperation({ summary: 'Add group for student' })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'UUID of the student',
  })
  @ApiBody({
    type: AddGroupDto,
    description: 'Add group for student',
    examples: {
      base: {
        summary: 'Example of adding group for student',
        value: {
          groupId: '629f9340-e335-4af6-8555-1c5439e79403',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Add group for student',
    type: GetStudentWithGroupDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid student id | Validation failed',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found | Group not found',
    schema: {
      example: {
        message: 'Student not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Patch(':id/group')
  addGroupForStudent(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() body: { groupId: AddGroupDto },
  ): Promise<GetStudentWithGroupDto> {
    return this.studentsControllerService.addGroupForStudent(id, body.groupId);
  }

  @ApiOperation({ summary: 'Delete student' })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'UUID of the student',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Student was delete',
    type: GetStudentDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid student id',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found',
    schema: {
      example: {
        message: 'Student not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Delete(':id')
  removeStudent(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<GetStudentDto> {
    return this.studentsControllerService.removeStudent(id);
  }

  @ApiOperation({ summary: "Get student's marks" })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'UUID of the student',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: "Get student's marks",
    type: GetStudentMarkDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid student id',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found',
    schema: {
      example: {
        message: 'Student not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Get(':id/marks')
  getStudentMarks(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<GetStudentMarkDto[]> {
    return this.studentsControllerService.getStudentMarks(id);
  }

  @ApiOperation({ summary: 'Add image for student' })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'UUID of the student',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Image successfully load',
    type: GetStudentDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid student id | File is not provided',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found',
    schema: {
      example: {
        message: 'Student not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Patch(':id/image')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: editFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  async addImage(
    @Param('id', ParseUUIDPipe) id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.studentsControllerService.addImage(id, file.filename);
  }

  @ApiOperation({ summary: 'Add course for student' })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'UUID of the student',
  })
  @ApiBody({
    type: GetStudentWithCourseDto,
    description: 'Add course for student',
    examples: {
      based: {
        summary: 'Example of adding course for student',
        value: {
          courseId: '62cbb833-fbe5-4e8e-b0a9-a1dd7a693630',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Create new student',
    type: GetStudentDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed | Invalid student id',
    schema: {
      example: {
        message: 'Validation failed (uuid is expected)',
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Student not found | Course not found',
    schema: {
      example: {
        message: 'Student not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Patch(':id/course')
  addCourseForStudent(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() courseIdDto: AddCourseDto,
  ): Promise<GetStudentWithCourseDto> {
    return this.studentsControllerService.addCourseForStudent(id, courseIdDto);
  }
}
