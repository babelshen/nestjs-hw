import { Injectable } from '@nestjs/common';
import { GroupsService } from '../../services/groups/groups.service';
import { CreateGroupDto } from '../../services/groups/dto/create-group.dto';
import { UpdateGroupDto } from '../../services/groups/dto/update-group.dto';
import { QueryFilterDto } from '../../application/dto/query.filter.dto';

@Injectable()
export class GroupsControllerService {
  constructor(private readonly groupsService: GroupsService) {}

  createGroup(createGroupDto: CreateGroupDto) {
    return this.groupsService.createGroup(createGroupDto);
  }

  findAllGroups(queryFilter?: QueryFilterDto) {
    return this.groupsService.findAllGroups(queryFilter);
  }

  findOneGroupWithStudents(id: string) {
    return this.groupsService.findOneGroupWithStudents(id);
  }

  updateGroup(id: string, updateGroupDto: UpdateGroupDto) {
    return this.groupsService.updateGroup(id, updateGroupDto);
  }

  removeGroup(id: string) {
    return this.groupsService.removeGroup(id);
  }
}
