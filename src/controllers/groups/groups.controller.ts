import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseUUIDPipe,
  HttpStatus,
  UseGuards,
  Query,
} from '@nestjs/common';
import { GroupsControllerService } from './groups.controller.service';
import { CreateGroupDto } from '../../services/groups/dto/create-group.dto';
import { UpdateGroupDto } from '../../services/groups/dto/update-group.dto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { GetGroupDto } from '../../services/groups/dto/get-group.response.dto';
import { GetGroupWithStudentsDto } from '../../services/groups/dto/get-group-with-students.response.dto';
import {
  InternalServerError,
  NonCorrectId,
  Unauthorized,
} from '../../application/constants/errors.constants';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@ApiTags('Groups')
@UseGuards(AuthGuard)
@Controller('groups')
@ApiBearerAuth('jwt')
export class GroupsController {
  constructor(
    private readonly groupsControllerService: GroupsControllerService,
  ) {}

  @ApiOperation({ summary: 'Create new group' })
  @ApiBody({
    type: CreateGroupDto,
    description: 'Create group request',
    examples: {
      based: {
        summary: 'Example create group',
        value: {
          name: 'Gryffindor',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create new group',
    type: GetGroupDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Dublicate name | Validation failed',
    schema: {
      example: {
        message: [
          'The name should not be more than fifty characters',
          'The name must contain a minimum of five characters',
          'name must be a string',
        ],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Post()
  createGroup(@Body() createGroupDto: CreateGroupDto): Promise<GetGroupDto> {
    console.log(createGroupDto);
    return this.groupsControllerService.createGroup(createGroupDto);
  }

  @ApiOperation({
    summary: 'Get all groups',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List of groups',
    type: GetGroupDto,
    isArray: true,
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @ApiQuery({
    name: 'name',
    required: false,
    type: String,
    description: 'Name of the group',
    example: 'Gryffindor',
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: String,
    description: 'Order by field',
    example: 'DESC',
  })
  @ApiQuery({
    name: 'sortField',
    required: false,
    type: String,
    description: 'Sort by field',
    example: 'name',
  })
  @Get()
  findAllGroups(@Query() queryFilter?: QueryFilterDto): Promise<GetGroupDto[]> {
    return this.groupsControllerService.findAllGroups(queryFilter);
  }

  @ApiOperation({ summary: 'Get group by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get group by id',
    type: GetGroupWithStudentsDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid group id',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Group not found',
    schema: {
      example: {
        message: 'Group not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the group' })
  @Get(':id')
  findOneGroupWithStudents(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<GetGroupWithStudentsDto> {
    return this.groupsControllerService.findOneGroupWithStudents(id);
  }

  @ApiOperation({ summary: 'Update group by id' })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the group' })
  @ApiBody({
    type: UpdateGroupDto,
    description: 'Request for updating params of group data',
    examples: {
      based: {
        summary: 'Update group name',
        value: {
          name: 'Slytherin',
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Update group by id',
    type: GetGroupWithStudentsDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid group id | Validation failed',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Group not found',
    schema: {
      example: {
        message: 'Group not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Patch(':id')
  updateGroup(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateGroupDto: UpdateGroupDto,
  ): Promise<GetGroupWithStudentsDto> {
    return this.groupsControllerService.updateGroup(id, updateGroupDto);
  }

  @ApiOperation({ summary: 'Delete group' })
  @ApiParam({
    name: 'id',
    type: String,
    description: 'UUID of the group',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Group was delete',
    type: GetGroupWithStudentsDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid group id',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Group not found',
    schema: {
      example: {
        message: 'Group not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Delete(':id')
  removeGroup(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<GetGroupWithStudentsDto> {
    return this.groupsControllerService.removeGroup(id);
  }
}
