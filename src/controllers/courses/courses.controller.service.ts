import { Injectable } from '@nestjs/common';
import { CoursesService } from '../../services/courses/courses.service';
import { CreateCourseDto } from '../../services/courses/dto/create-course.dto';
import { QueryFilterDto } from '../../application/dto/query.filter.dto';
import { UpdateCourseDto } from '../../services/courses/dto/update-course.dto';

@Injectable()
export class CoursesControllerService {
  constructor(private readonly coursesService: CoursesService) {}

  createCourse(createCourseDto: CreateCourseDto) {
    return this.coursesService.createCourse(createCourseDto);
  }

  findAllCourses(queryFilter?: QueryFilterDto) {
    return this.coursesService.findAllCourses(queryFilter);
  }

  findOneCourse(id: string) {
    return this.coursesService.findOneCourse(id);
  }

  getCourseMarks(id: string) {
    return this.coursesService.getCourseMarks(id);
  }

  updateCourse(id: string, updateCourseDto: UpdateCourseDto) {
    return this.coursesService.updateCourse(id, updateCourseDto);
  }
}
