import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  ParseUUIDPipe,
  UseGuards,
  HttpStatus,
  Query,
  Patch,
} from '@nestjs/common';
import { CreateCourseDto } from '../../services/courses/dto/create-course.dto';
import { CoursesControllerService } from './courses.controller.service';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { GetCourseDto } from '../../services/courses/dto/get-course.response.dto';
import { GetCourseMarksDto } from '../../services/courses/dto/get-course-marks.response.dto';
import {
  InternalServerError,
  NonCorrectId,
  Unauthorized,
} from '../../application/constants/errors.constants';
import { QueryFilterDto } from '../../application/dto/query.filter.dto';
import { GetCourseWithStudentsDto } from '../../services/courses/dto/get-course-with-students.dto';
import { UpdateCourseDto } from 'src/services/courses/dto/update-course.dto';

@ApiTags('Courses')
@UseGuards(AuthGuard)
@Controller('courses')
@ApiBearerAuth('jwt')
export class CoursesController {
  constructor(
    private readonly coursesControllerService: CoursesControllerService,
  ) {}

  @ApiSecurity('bearerAuth')
  @ApiOperation({ summary: 'Create new course' })
  @ApiBody({
    type: CreateCourseDto,
    description: 'Create course request',
    examples: {
      based: {
        summary: 'Example of create course',
        value: {
          name: 'Magic',
          description: 'Magic science',
          hours: 15,
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create new course',
    type: GetCourseDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Dublicate name | Validation failed',
    schema: {
      example: {
        message: [
          'The name should not be more than fifty characters',
          'The name must contain a minimum of three characters',
          'name must be a string',
          'The description should not be more than fifty characters',
          'The description must contain a minimum of five characters',
          'description must be a string',
          'The maximum number of hours is two hundred',
          'The minimum number of hours is ten',
          'hours must be a number conforming to the specified constraints',
        ],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Post()
  createCourse(
    @Body() createCourseDto: CreateCourseDto,
  ): Promise<GetCourseDto> {
    return this.coursesControllerService.createCourse(createCourseDto);
  }

  @ApiSecurity('bearerAuth')
  @ApiOperation({ summary: 'Get all courses' })
  @ApiQuery({
    name: 'name',
    required: false,
    type: String,
    description: 'Name of the course',
    example: 'Albus',
  })
  @ApiQuery({
    name: 'sortOrder',
    required: false,
    type: String,
    description: 'Order by field',
    example: 'DESC',
  })
  @ApiQuery({
    name: 'sortField',
    required: false,
    type: String,
    description: 'Sort by field',
    example: 'name',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List of courses',
    isArray: true,
    type: GetCourseWithStudentsDto,
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Get()
  findAllCourses(
    @Query() queryFilter?: QueryFilterDto,
  ): Promise<GetCourseWithStudentsDto[]> {
    return this.coursesControllerService.findAllCourses(queryFilter);
  }

  @ApiOperation({ summary: 'Get course by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Course by id',
    type: GetCourseDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid course id',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course not found',
    schema: {
      example: {
        message: 'Course not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the course' })
  @Get(':id')
  findOneCourse(@Param('id', ParseUUIDPipe) id: string): Promise<GetCourseDto> {
    return this.coursesControllerService.findOneCourse(id);
  }

  @ApiOperation({ summary: 'Get marks for course' })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the course' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List of marks for course',
    isArray: true,
    type: GetCourseMarksDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Invalid course id',
    schema: {
      example: NonCorrectId,
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course not found',
    schema: {
      example: {
        message: 'Course not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @Get(':id/marks')
  getCourseMarks(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<GetCourseMarksDto[]> {
    return this.coursesControllerService.getCourseMarks(id);
  }

  @ApiOperation({ summary: 'Update course by id' })
  @ApiBody({
    type: UpdateCourseDto,
    description: 'Update course request',
    examples: {
      based: {
        summary: 'Example of update course',
        value: {
          name: 'Magic',
          description: 'Magic science',
          hours: 15,
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Update course by id',
    type: GetCourseDto,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Validation failed | Invalid course id',
    schema: {
      example: {
        message: [
          'The name should not be more than fifty characters',
          'The name must contain a minimum of three characters',
          'name must be a string',
        ],
        error: 'Bad Request',
        statusCode: 400,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'Unauthorized',
    schema: {
      example: Unauthorized,
    },
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Course not found',
    schema: {
      example: {
        message: 'Course not found',
        error: 'Not Found',
        statusCode: 404,
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    description: 'Internal Server Error',
    schema: {
      example: InternalServerError,
    },
  })
  @ApiParam({ name: 'id', type: String, description: 'UUID of the course' })
  @Patch(':id')
  updateCourse(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateCourseDto: UpdateCourseDto,
  ): Promise<GetCourseDto> {
    return this.coursesControllerService.updateCourse(id, updateCourseDto);
  }
}
