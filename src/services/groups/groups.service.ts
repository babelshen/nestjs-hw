import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { Group } from './entities/group.entity';
import { GetGroupDto } from './dto/get-group.response.dto';
import { GetGroupWithStudentsDto } from './dto/get-group-with-students.response.dto';
import { QueryFilterDto } from '../../application/dto/query.filter.dto';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupRepository: Repository<Group>,
  ) {}

  public async createGroup(
    createGroupDto: CreateGroupDto,
  ): Promise<GetGroupDto> {
    try {
      return await this.groupRepository.save(createGroupDto);
    } catch (e) {
      throw new BadRequestException('Group with this name already exists');
    }
  }

  public async findAllGroups(
    queryFilter?: QueryFilterDto,
  ): Promise<GetGroupDto[]> {
    try {
      const where: Record<string, any> = {};

      if (queryFilter) {
        if (queryFilter.name) {
          where.name = ILike(`%${queryFilter.name}%`);
        }

        if (queryFilter.sortField && queryFilter.sortOrder) {
          const order = {
            [queryFilter.sortField]: queryFilter.sortOrder,
          };

          const groups = await this.groupRepository.find({
            where,
            order,
          });

          return groups;
        }
      }

      const groups = await this.groupRepository.find({
        where,
      });

      return groups;
    } catch (e) {
      throw e;
    }
  }

  public async findOneGroupWithStudents(
    id: string,
  ): Promise<GetGroupWithStudentsDto> {
    try {
      const group = await this.groupRepository
        .createQueryBuilder('group')
        .leftJoinAndSelect('group.students', 'student')
        .where('group.id = :id', { id })
        .getOne();
      if (!group) throw new NotFoundException('Group not found');
      return group;
    } catch (e) {
      throw e;
    }
  }

  public async findOneGroup(id: string): Promise<GetGroupDto> {
    try {
      const group = await this.groupRepository.findOne({ where: { id } });
      if (!group) throw new NotFoundException('Group not found');
      return group;
    } catch (e) {
      throw e;
    }
  }

  public async updateGroup(
    id: string,
    updateGroupDto: UpdateGroupDto,
  ): Promise<GetGroupWithStudentsDto> {
    try {
      await this.groupRepository.update(id, updateGroupDto);
      const result = await this.findOneGroup(id);
      return result;
    } catch (e) {
      throw e;
    }
  }

  public async removeGroup(id: string): Promise<GetGroupWithStudentsDto> {
    try {
      const result = await this.findOneGroup(id);
      await this.groupRepository.delete(id);
      return result;
    } catch (e) {
      throw e;
    }
  }
}
