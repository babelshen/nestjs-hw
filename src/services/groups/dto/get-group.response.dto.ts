import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class GetGroupDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Gryffindor',
    type: String,
    description: 'The name of the group',
  })
  name: string;

  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: '2090f28f-0a2f-4642-8851-7237ae2c57d1',
    type: String,
    description: 'UUID of the group',
  })
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T09:04:24.062Z',
    type: Date,
    description: 'UUID of the group',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T09:04:24.062Z',
    type: Date,
    description: 'UUID of the group',
  })
  updatedAt: Date;
}
