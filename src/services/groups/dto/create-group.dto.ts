import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateGroupDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(5, {
    message: 'The name must contain a minimum of five characters',
  })
  @MaxLength(50, {
    message: 'The name should not be more than fifty characters',
  })
  @ApiProperty({
    example: 'Gryffindor',
    type: String,
    description: 'The name of the group',
    minLength: 5,
    maxLength: 50,
  })
  name: string;
}
