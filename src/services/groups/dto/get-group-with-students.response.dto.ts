import { ApiProperty } from '@nestjs/swagger';
import { Student } from '../../students/entities/student.entity';
import { IsArray, IsDate, IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class GetGroupWithStudentsDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Gryffindor',
    type: String,
    description: 'The name of the group',
  })
  name: string;

  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: '2090f28f-0a2f-4642-8851-7237ae2c57d1',
    type: String,
    description: 'UUID of the group',
  })
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T09:04:24.062Z',
    type: Date,
    description: 'UUID of the group',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T09:04:24.062Z',
    type: Date,
    description: 'UUID of the group',
  })
  updatedAt: Date;

  @IsArray()
  @ApiProperty({
    example: [
      {
        id: '96608d8e-706f-49ca-a036-fb5150a63dce',
        createdAt: '2023-08-27T10:04:45.904Z',
        updatedAt: '2023-08-27T10:10:01.317Z',
        name: 'Germiona',
        surname: 'Glister',
        email: 'fffff@mail.com',
        age: 17,
        imagePath: null,
        groupId: '2090f28f-0a2f-4642-8851-7237ae2c57d1',
      },
    ],
    type: Student,
    description: 'List of students who are in the group',
  })
  student?: Student[];
}
