import { Column, Entity, Index, OneToMany } from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Student } from '../../students/entities/student.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'groups' })
export class Group extends CoreEntity {
  @ApiProperty({
    example: 'Gryffindor',
    type: String,
    description: 'The name of the group',
  })
  @Index()
  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  name: string;

  @OneToMany(() => Student, (student) => student.group, { cascade: ['remove'] })
  students: Student[];
}
