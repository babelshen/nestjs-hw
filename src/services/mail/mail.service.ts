import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailService {
  constructor(private readonly mailerService: MailerService) {}

  public async sendMail(token: string): Promise<void> {
    try {
      await this.mailerService.sendMail({
        to: 'ilyaakulov96@gmail.com',
        from: 'no-reply@gmail.com',
        subject: 'Reset password',
        template: 'reset-password',
        context: {
          token,
        },
      });
    } catch (e) {
      throw e;
    }
  }
}
