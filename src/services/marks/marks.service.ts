import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Mark } from './entities/mark.entity';
import { LectorsService } from '../lectors/lectors.service';
import { AddMarktDto } from './dto/add-mark-for-student.dto';
import { CoursesService } from '../courses/courses.service';
import { StudentsService } from '../students/students.service';

@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Mark)
    private readonly markRepository: Repository<Mark>,
    private readonly lectorService: LectorsService,
    private readonly courseService: CoursesService,
    private readonly studentService: StudentsService,
  ) {}

  public async addMark(addMarkDto: AddMarktDto): Promise<AddMarktDto> {
    try {
      await this.lectorService.findOneLector(addMarkDto.lector_id);
      await this.courseService.findOneCourse(addMarkDto.course_id);
      await this.studentService.findOneStudent(addMarkDto.student_id);

      const mark = {
        mark: addMarkDto.mark,
        lector_id: addMarkDto.lector_id,
        course_id: addMarkDto.course_id,
        student_id: addMarkDto.student_id,
      };

      return this.markRepository.save(mark);
    } catch (e) {
      throw e;
    }
  }
}
