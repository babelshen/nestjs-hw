import { Course } from '../../../services/courses/entities/course.entity';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { Student } from '../../../services/students/entities/student.entity';
import { Lector } from '../../../services/lectors/entities/lector.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @ManyToOne(() => Course, (course) => course.marks, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'course_id' })
  course: string;

  @ManyToOne(() => Student, (student) => student.marks, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'student_id' })
  student: string;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'lector_id' })
  lector: string;

  @ApiProperty({
    example: 10,
    type: Number,
    description: 'The mark of the student for course from lector',
  })
  @Column({
    type: 'integer',
    nullable: true,
  })
  mark: number;
}
