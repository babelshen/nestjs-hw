import { IsNotEmpty, IsNumber, IsUUID, Max, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AddMarktDto {
  @IsNotEmpty()
  @IsNumber()
  @Min(1, {
    message: 'The minimum mark cannot be less than 1',
  })
  @Max(12, {
    message: 'The maximum mark cannot be more than 12',
  })
  @ApiProperty({
    example: 10,
    type: Number,
    description: 'Mark value',
  })
  mark: number;

  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: 'f5216192-ac9c-4490-b0e2-fae55fecb969',
    type: String,
    description: 'UUID of the lector',
  })
  lector_id: string;

  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: '88087287-b2c0-4d39-ad5d-107cd9fc6cf3',
    type: String,
    description: 'UUID of the course',
  })
  course_id: string;

  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: '96608d8e-706f-49ca-a036-fb5150a63dce',
    type: String,
    description: 'UUID of the student',
  })
  student_id: string;
}
