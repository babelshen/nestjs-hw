import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MarksService } from './marks.service';
import { Mark } from './entities/mark.entity';
import { LectorsModule } from '../lectors/lectors.module';
import { CoursesModule } from '../courses/courses.module';
import { StudentsModule } from '../students/students.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Mark]),
    StudentsModule,
    LectorsModule,
    CoursesModule,
  ],
  providers: [MarksService],
  exports: [MarksService],
})
export class MarksModule {}
