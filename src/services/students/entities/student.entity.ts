import {
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Course } from '../../courses/entities/course.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Index()
  @ApiProperty({
    example: 'Garry',
    type: String,
    description: 'The name of the student',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @ApiProperty({
    example: 'Potter',
    type: String,
    description: 'The surname of the student',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @ApiProperty({
    example: 'garrypatter@gmail.com',
    type: String,
    description: 'The email of the student',
  })
  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  email: string;

  @ApiProperty({
    example: 18,
    type: Number,
    description: 'The age of the student',
  })
  @Column({
    type: 'numeric',
    nullable: false,
  })
  age: number;

  @ApiPropertyOptional({
    example: '0940304.png',
    type: String,
    description: 'The path of the image',
  })
  @Column({
    type: 'varchar',
    nullable: true,
    name: 'image_path',
  })
  imagePath: string;

  @ApiPropertyOptional({
    example: '63105bf6-d710-4f19-9321-70b937f2353e',
    type: String,
    description: 'The group id of the student',
  })
  @Column({
    type: 'uuid',
    nullable: true,
    name: 'group_id',
  })
  groupId: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: true,
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @OneToMany(() => Mark, (mark) => mark.student, { cascade: ['remove'] })
  marks: Mark[];

  @ManyToMany(() => Course, (course) => course.students, {
    nullable: true,
    onDelete: 'CASCADE',
    onUpdate: 'NO ACTION',
  })
  @JoinTable({
    name: 'student_course',
    joinColumn: {
      name: 'student_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses?: Course[];
}
