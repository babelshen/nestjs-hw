import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
  Max,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CreateStudentDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(3, {
    message: 'The name must contain a minimum of three characters',
  })
  @MaxLength(50, {
    message: 'The name should not be more than fifty characters',
  })
  @ApiProperty({
    example: 'Garry',
    type: String,
    description: 'The name of the student',
    minLength: 3,
    maxLength: 50,
  })
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @MinLength(5, {
    message: 'The email must contain a minimum of five characters',
  })
  @MaxLength(50, {
    message: 'The email should not be more than fifty characters',
  })
  @ApiProperty({
    example: 'garrypotter@gmail.com',
    type: String,
    description: 'The email of the student',
    minLength: 5,
    maxLength: 50,
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3, {
    message: 'The surname must contain a minimum of three characters',
  })
  @MaxLength(50, {
    message: 'The surname should not be more than fifty characters',
  })
  @ApiProperty({
    example: 'Potter',
    type: String,
    description: 'The surname of the student',
    minLength: 5,
    maxLength: 50,
  })
  surname: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(16, {
    message: 'The value of the age field cannot be less than 16',
  })
  @Max(65, {
    message: 'The value of the age field cannot be more than 65',
  })
  @ApiProperty({
    example: 18,
    type: Number,
    description: 'The age of the student',
  })
  age: number;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    example: '0908050505.png',
    required: false,
    description: 'Path to the image',
  })
  imagePath: string;

  @IsOptional()
  @IsUUID()
  @ApiPropertyOptional({
    example: '2090f28f-0a2f-4642-8851-7237ae2c57d1',
    required: false,
    description: 'UUID of group',
  })
  groupId?: string;
}
