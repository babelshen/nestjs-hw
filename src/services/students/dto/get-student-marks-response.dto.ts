import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetStudentMarkDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Garry',
    type: String,
    description: 'The name of the student',
    minLength: 3,
    maxLength: 50,
  })
  studentName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Potter',
    type: String,
    description: 'The surname of the student',
    minLength: 5,
    maxLength: 50,
  })
  studentSurname: string;

  @IsString()
  @ApiProperty({
    example: 'Magic Science',
    type: String,
    description: 'Name of course (can be null)',
  })
  courseName: string | null;

  @IsString()
  @ApiProperty({
    example: 10,
    type: Number,
    description: 'Mark of student for course (can be null)',
  })
  mark: string | null;
}
