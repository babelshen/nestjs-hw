import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AddCourseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '2090f28f-0a2f-4642-8851-7237ae2c57d1',
    type: String,
    description: 'UUID of the course',
  })
  courseId: string;
}
