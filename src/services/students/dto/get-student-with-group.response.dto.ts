import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { GetGroupDto } from '../../groups/dto/get-group.response.dto';
import {
  IsDate,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
} from 'class-validator';

export class GetStudentWithGroupDto {
  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: 'b0e31418-e591-43c5-bbe5-de4990a519d7',
    type: String,
    description: 'UUID of the student',
  })
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Creation date and time of the student',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Last update date and time of the student',
  })
  updatedAt: Date;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Garry',
    type: String,
    description: 'The name of the student',
  })
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'garrypotter@gmail.com',
    type: String,
    description: 'The email of the student',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Potter',
    type: String,
    description: 'The surname of the student',
  })
  surname: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    example: 18,
    type: Number,
    description: 'The age of the student',
  })
  age: number;

  @IsString()
  @ApiPropertyOptional({
    example: '098475930.png',
    type: String,
    required: false,
    description: 'Image path (can be null)',
  })
  imagePath?: string | null;

  @ApiProperty({
    example: {
      id: '2090f28f-0a2f-4642-8851-7237ae2c57d1',
      createdAt: '2023-08-27T09:04:24.062Z',
      updatedAt: '2023-08-27T09:04:24.062Z',
      name: 'Gryffindor',
      students: [],
    },
    type: GetGroupDto,
    description: 'Information about adding group',
  })
  group: GetGroupDto;
}
