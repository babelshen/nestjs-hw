import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, Repository } from 'typeorm';
import { Student } from './entities/student.entity';
import { GroupsService } from '../groups/groups.service';
import { AddGroupDto } from './dto/add-group-for-student.dto';
import { GetStudentMarkDto } from './dto/get-student-marks-response.dto';
import { GetStudentDto } from './dto/get-students-response.dto';
import { GetStudentWithGroupDto } from './dto/get-student-with-group.response.dto';
import { CoursesService } from '../courses/courses.service';
import { AddCourseDto } from './dto/add-course-for-student.dto';
import { GetStudentWithCourseDto } from './dto/get-student-with-course.dto';
import { Course } from '../courses/entities/course.entity';
import { QueryFilterDto } from '../../application/dto/query.filter.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
    private readonly groupService: GroupsService,
    private readonly courseService: CoursesService,
  ) {}

  public async createStudent(
    createStudentDto: CreateStudentDto,
  ): Promise<GetStudentDto> {
    try {
      const { email, groupId } = createStudentDto;

      if (groupId) await this.groupService.findOneGroup(groupId);

      const student = await this.studentRepository.findOne({
        where: { email },
      });
      if (student)
        throw new BadRequestException('Student with this email already exists');

      return await this.studentRepository.save(createStudentDto);
    } catch (e) {
      throw e;
    }
  }

  public async findAllStudents(
    queryFilter?: QueryFilterDto,
  ): Promise<GetStudentDto[]> {
    try {
      const studentsQuery = this.studentRepository
        .createQueryBuilder('student')
        .select([
          'student.id as id',
          'student.createdAt as "createdAt"',
          'student.updatedAt as "updatedAt"',
          'student.name as name',
          'student.surname as surname',
          'student.email as email',
          'student.age as age',
          'group.name as "groupName"',
          'student.imagePath as "imagePath"',
        ])
        .leftJoin('student.group', 'group');

      if (queryFilter) {
        if (queryFilter.name) {
          studentsQuery.andWhere(
            new Brackets((qb) => {
              qb.where('student.name ILIKE :searchText', {
                searchText: `%${queryFilter.name}%`,
              }).orWhere('student.surname ILIKE :searchText', {
                searchText: `%${queryFilter.name}%`,
              });
            }),
          );
        }

        if (queryFilter.sortField && queryFilter.sortOrder) {
          const order = {};
          order[`student.${queryFilter.sortField}`] = queryFilter.sortOrder;
          studentsQuery.orderBy(order);
        }
      }

      const studentsWithGroups = await studentsQuery.getRawMany();

      const studentsWithCourses = [];
      for (const student of studentsWithGroups) {
        const coursesQuery = await this.studentRepository
          .createQueryBuilder('student')
          .relation('courses')
          .of(student.id)
          .loadMany();

        const courses = coursesQuery.map((course) => course.name);

        studentsWithCourses.push({ ...student, courses });
      }

      return studentsWithCourses;
    } catch (e) {
      throw e;
    }
  }

  public async findOneStudent(id: string): Promise<GetStudentWithGroupDto> {
    try {
      const student = await this.studentRepository
        .createQueryBuilder('student')
        .select([
          'student.id as id',
          'student.createdAt as "createdAt"',
          'student.updatedAt as "updatedAt"',
          'student.name as name',
          'student.surname as surname',
          'student.email as email',
          'student.age as age',
          'student.imagePath as "imagePath"',
        ])
        .leftJoin('student.group', 'group')
        .addSelect('group.name as "groupName"')
        .where('student.id = :id', { id })
        .getRawOne();

      if (!student) throw new NotFoundException('Student not found');

      return student;
    } catch (e) {
      throw e;
    }
  }

  public async updateStudent(
    id: string,
    updateStudentDto: UpdateStudentDto,
  ): Promise<GetStudentDto> {
    try {
      const { groupId } = updateStudentDto;

      if (groupId) await this.groupService.findOneGroup(groupId);

      await this.studentRepository.update(id, updateStudentDto);
      return await this.findOneStudent(id);
    } catch (e) {
      throw e;
    }
  }

  public async removeStudent(id: string): Promise<GetStudentDto> {
    try {
      const result = await this.findOneStudent(id);
      await this.studentRepository.delete(id);
      return result;
    } catch (e) {
      throw e;
    }
  }

  public async addGroupForStudent(
    id: string,
    groupIdDto: AddGroupDto,
  ): Promise<GetStudentWithGroupDto> {
    try {
      const student = await this.findOneStudent(id);
      const group = await this.groupService.findOneGroup(groupIdDto.groupId);
      student.group = group;
      return this.studentRepository.save(student);
    } catch (e) {
      throw e;
    }
  }

  public async addCourseForStudent(
    id: string,
    courseIdDto: AddCourseDto,
  ): Promise<GetStudentWithCourseDto> {
    try {
      const student = await this.studentRepository
        .createQueryBuilder('student')
        .where('student.id = :id', { id })
        .leftJoinAndSelect('student.courses', 'courses')
        .getOne();

      const course = await this.courseService.findOneCourse(
        courseIdDto.courseId,
      );

      if (!student) {
        throw new NotFoundException('Student not found');
      }

      const doubleCourse = student.courses.find(
        (c) => c.id === courseIdDto.courseId,
      );

      if (doubleCourse) {
        throw new BadRequestException(
          'The course has already been added to the student',
        );
      }

      student.courses.push(course as Course);
      await this.studentRepository.save(student);

      return student;
    } catch (e) {
      throw e;
    }
  }

  public async getStudentMarks(id: string): Promise<GetStudentMarkDto[]> {
    try {
      await this.findOneStudent(id);
      const student = await this.studentRepository
        .createQueryBuilder('student')
        .leftJoinAndSelect('student.marks', 'mark')
        .leftJoinAndSelect('mark.course', 'course')
        .select([
          'student.name AS "studentName"',
          'student.surname AS "studentSurname"',
          'course.name AS "courseName"',
          'mark.mark AS mark',
        ])
        .where('student.id = :id', { id })
        .getRawMany();
      return student;
    } catch (e) {
      throw e;
    }
  }

  public async addImage(
    id: string,
    filePath?: string,
  ): Promise<GetStudentWithGroupDto> {
    if (!filePath) {
      throw new BadRequestException('File is not provided');
    }

    try {
      const schema = {
        imagePath: filePath,
      };

      await this.updateStudent(id, schema);
      const student = await this.findOneStudent(id);
      return student;
    } catch (e) {
      throw e;
    }
  }
}
