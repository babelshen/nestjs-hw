import { Module } from '@nestjs/common';
import { StudentsService } from './students.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Student } from './entities/student.entity';
import { GroupsModule } from '../groups/groups.module';
import { CoursesModule } from '../courses/courses.module';

@Module({
  imports: [TypeOrmModule.forFeature([Student]), GroupsModule, CoursesModule],
  providers: [StudentsService],
  exports: [StudentsService],
})
export class StudentsModule {}
