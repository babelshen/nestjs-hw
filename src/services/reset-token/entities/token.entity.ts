import { ApiProperty } from '@nestjs/swagger';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Column, Entity } from 'typeorm';

@Entity({ name: 'tokens' })
export class Token extends CoreEntity {
  @ApiProperty({
    example: 'garrypotter@gmail.com',
    type: String,
    description: 'The email of the user',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @ApiProperty({
    example: 'baf843f4e3a6934f4f04849113323e8eb47625a2a5bcf31dcdb0ce08faf97ae0',
    type: String,
    description: 'Token',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  token: string;
}
