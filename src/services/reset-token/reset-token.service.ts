import crypto from 'crypto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Token } from './entities/token.entity';
import { MailService } from '../mail/mail.service';
import { GetAccessTokenDto } from './dto/get-access-token.dto';

@Injectable()
export class ResetTokenService {
  constructor(
    @InjectRepository(Token)
    private readonly resetTokenRepository: Repository<Token>,
    private readonly mailService: MailService,
  ) {}

  public async generateResetToken(email: string): Promise<GetAccessTokenDto> {
    try {
      const token = crypto.randomBytes(32).toString('hex');
      await this.mailService.sendMail(token);
      return await this.resetTokenRepository.save({ email, token });
    } catch (e) {
      throw e;
    }
  }

  public async getResetToken(
    token: string,
  ): Promise<GetAccessTokenDto | undefined> {
    try {
      return await this.resetTokenRepository.findOne({ where: { token } });
    } catch (e) {
      throw e;
    }
  }

  public async removeResetToken(token: string): Promise<DeleteResult> {
    try {
      return await this.resetTokenRepository.delete({ token });
    } catch (e) {
      throw e;
    }
  }
}
