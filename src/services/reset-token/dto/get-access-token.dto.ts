import { IsDate, IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetAccessTokenDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'b0e31418-e591-43c5-bbe5-de4990a519d7',
    type: String,
    description: 'UUID of the token',
  })
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Creation date and time of the token',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Last update date and time of the token',
  })
  updatedAt: Date;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'garrypotter@gmail.com',
    type: String,
    description: 'The email of the user',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'baf843f4e3a6934f4f04849113323e8eb47625a2a5bcf31dcdb0ce08faf97ae0',
    type: String,
    description: 'Token',
  })
  token: string;
}
