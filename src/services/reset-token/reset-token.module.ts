import { Module } from '@nestjs/common';
import { ResetTokenService } from './reset-token.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Token } from './entities/token.entity';
import { MailModule } from '../mail/mail.module';

@Module({
  imports: [TypeOrmModule.forFeature([Token]), MailModule],
  providers: [ResetTokenService],
  exports: [ResetTokenService],
})
export class ResetTokenModule {}
