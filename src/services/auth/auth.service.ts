import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ResetTokenService } from '../reset-token/reset-token.service';
import { SignResponseDto } from '../../controllers/auth/dto/sign.response.dto';
import { ResetTokenInterface } from '../reset-token/interfaces/reset-token.interface';
import { ResetPasswordWithTokenRequestDto } from '../../controllers/auth/dto/reset-password-with-token.request.dto';
import { LectorsService } from '../lectors/lectors.service';
import * as argon2 from 'argon2';
import { SignUpRequestDto } from '../../controllers/auth/dto/sign-up.request.dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
    private lectorService: LectorsService,
  ) {}

  public async signUp(signUpDto: SignUpRequestDto): Promise<SignResponseDto> {
    const lector = {
      name: signUpDto.username,
      email: signUpDto.email,
      password: signUpDto.password,
    };
    const user = await this.lectorService.createLector(lector);
    const payload = { userId: user.id, username: user.name };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async resetPasswordRequest(
    email: string,
  ): Promise<ResetTokenInterface> {
    const user = await this.lectorService.findOneLectorByEmail(email);
    if (!user) {
      throw new BadRequestException(
        `Cannot generate token for reset password request because account is not found`,
      );
    }
    return await this.resetTokenService.generateResetToken(email);
  }

  public async resetPassword(
    resetPasswordWithTokenRequestDto: ResetPasswordWithTokenRequestDto,
  ): Promise<void> {
    const { token, email, oldPassword, newPassword } =
      resetPasswordWithTokenRequestDto;
    const resetPasswordRequest = await this.resetTokenService.getResetToken(
      token,
    );
    if (!resetPasswordRequest) {
      throw new BadRequestException(`There is no request password request`);
    }
    const user = await this.lectorService.findOneLectorByEmail(email);
    if (!user) {
      throw new BadRequestException(`Lector is not found`);
    }
    const verification = await argon2.verify(user.password, oldPassword);
    if (!verification) {
      throw new BadRequestException(`Old password is incorrect`);
    }

    await this.lectorService.updateLectorPassword(user.id, newPassword);
    await this.resetTokenService.removeResetToken(token);
  }

  public async signIn(email: string, pass: string): Promise<SignResponseDto> {
    try {
      const user = await this.lectorService.findOneLectorByEmail(email);
      const verification = await argon2.verify(user.password, pass);
      if (!verification) {
        throw new UnauthorizedException('Incorrect email or password');
      }
      const payload = { userId: user.id, username: user.name };
      return {
        accessToken: await this.jwtService.signAsync(payload),
      };
    } catch (e) {
      throw e;
    }
  }
}
