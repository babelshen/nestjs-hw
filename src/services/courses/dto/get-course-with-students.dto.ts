import { ApiProperty } from '@nestjs/swagger';
import {
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
} from 'class-validator';

export class GetCourseWithStudentsDto {
  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: '45ee8032-16fe-414e-a164-37fd76997f0f',
    type: String,
    description: 'UUID of the course',
  })
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Creation date and time of the course',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Last update date and time of the course',
  })
  updatedAt: Date;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Magic',
    type: String,
    description: 'The name of the course',
  })
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Magic science',
    type: String,
    description: 'The description of the course',
  })
  description: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    example: 20,
    type: Number,
    description: 'The hours of the course',
  })
  hours: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    example: 2,
    type: Number,
    description: 'The number of students',
  })
  studentCount: number;
}
