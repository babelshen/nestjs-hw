import {
  IsNotEmpty,
  IsNumber,
  IsString,
  Max,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCourseDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(3, {
    message: 'The name must contain a minimum of three characters',
  })
  @MaxLength(50, {
    message: 'The name should not be more than fifty characters',
  })
  @ApiProperty({
    example: 'Magic',
    type: String,
    description: 'The name of course',
  })
  name: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(5, {
    message: 'The description must contain a minimum of five characters',
  })
  @MaxLength(50, {
    message: 'The description should not be more than fifty characters',
  })
  @ApiProperty({
    example: 'Magic science',
    type: String,
    description: 'The description of course',
  })
  description: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(10, {
    message: 'The minimum number of hours is ten',
  })
  @Max(200, {
    message: 'The maximum number of hours is two hundred',
  })
  @ApiProperty({
    example: 20,
    type: Number,
    description: 'Number of hours for studying the course',
  })
  hours: number;
}
