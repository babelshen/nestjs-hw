import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class GetCourseMarksDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Magic',
    type: String,
    description: 'The name of the course',
  })
  courseName: string;

  @IsString()
  @ApiProperty({
    example: 'Albus Dumbledore',
    type: String,
    description: 'The name of the lector',
  })
  lectorName: string;

  @IsString()
  @ApiProperty({
    example: 'Garry',
    type: String,
    description: 'The name of the student',
  })
  studentName: string;

  @IsString()
  @ApiProperty({
    example: 'Potter',
    type: String,
    description: 'The surname of the student',
  })
  studentSurname: string;

  @IsNumber()
  @ApiProperty({
    example: 12,
    type: Number,
    description: "Student's mark for course from lector",
  })
  mark: number;
}
