import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from './entities/course.entity';
import { ILike, Repository } from 'typeorm';
import { GetCourseMarksDto } from './dto/get-course-marks.response.dto';
import { GetCourseDto } from './dto/get-course.response.dto';
import { QueryFilterDto } from '../../application/dto/query.filter.dto';
import { GetCourseWithStudentsDto } from './dto/get-course-with-students.dto';
import { UpdateCourseDto } from './dto/update-course.dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
  ) {}

  public async createCourse(
    createCourseDto: CreateCourseDto,
  ): Promise<GetCourseDto> {
    try {
      return await this.courseRepository.save(createCourseDto);
    } catch (e) {
      throw new BadRequestException('Course with this name already exists');
    }
  }

  public async findAllCourses(
    queryFilter?: QueryFilterDto,
  ): Promise<GetCourseWithStudentsDto[]> {
    try {
      const where: Record<string, any> = {};

      if (queryFilter) {
        if (queryFilter.name) {
          where.name = ILike(`%${queryFilter.name}%`);
        }

        const order = {};

        if (queryFilter.sortField && queryFilter.sortOrder) {
          order[`course.${queryFilter.sortField}`] = queryFilter.sortOrder as
            | 'ASC'
            | 'DESC';
        }

        const courses = await this.courseRepository
          .createQueryBuilder('course')
          .where(where)
          .orderBy(order)
          .leftJoinAndSelect('course.students', 'students')
          .getMany();

        return courses.map((course) => ({
          id: course.id,
          createdAt: course.createdAt,
          updatedAt: course.updatedAt,
          name: course.name,
          description: course.description,
          hours: course.hours,
          studentCount: course.students.length,
        }));
      }

      const courses = await this.courseRepository
        .createQueryBuilder('course')
        .where(where)
        .leftJoinAndSelect('course.students', 'students')
        .getMany();

      return courses.map((course) => ({
        id: course.id,
        createdAt: course.createdAt,
        updatedAt: course.updatedAt,
        name: course.name,
        description: course.description,
        hours: course.hours,
        studentCount: course.students.length,
      }));
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  public async findOneCourse(id: string): Promise<GetCourseDto> {
    try {
      const course = await this.courseRepository.findOneBy({ id });
      if (!course) throw new NotFoundException('Course not found');
      return course;
    } catch (e) {
      throw e;
    }
  }

  public async getCourseMarks(id: string): Promise<GetCourseMarksDto[]> {
    try {
      const courses = await this.courseRepository
        .createQueryBuilder('course')
        .leftJoinAndSelect('course.marks', 'mark')
        .leftJoinAndSelect('mark.lector', 'lector')
        .leftJoinAndSelect('mark.student', 'student')
        .select([
          'course.name AS "courseName"',
          'lector.name AS "lectorName"',
          'student.name AS "studentName"',
          'student.surname AS "studentSurname"',
          'mark.mark AS mark',
        ])
        .where('course.id = :id', { id })
        .getRawMany();

      if (!courses.length) throw new NotFoundException('Courses not found');

      return courses;
    } catch (e) {
      throw e;
    }
  }

  public async updateCourse(
    id: string,
    updateCourseDto: UpdateCourseDto,
  ): Promise<GetCourseDto> {
    try {
      await this.courseRepository.update(id, updateCourseDto);
      const course = this.findOneCourse(id);
      return course;
    } catch (e) {
      throw e;
    }
  }
}
