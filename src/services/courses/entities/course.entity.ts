import { Column, Entity, Index, ManyToMany, OneToMany } from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Student } from '../../students/entities/student.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @Index()
  @ApiProperty({
    example: 'Magic',
    type: String,
    description: 'The name of the course',
  })
  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  name: string;

  @ApiProperty({
    example: 'Magic course',
    type: String,
    description: 'The description of the course',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  description: string;

  @ApiProperty({
    example: 48,
    type: Number,
    description: 'Number of hours for studying the course',
  })
  @Column({
    type: 'numeric',
    nullable: false,
  })
  hours: number;

  @ManyToMany(() => Lector, (lector) => lector.courses, {
    onDelete: 'CASCADE',
    onUpdate: 'NO ACTION',
  })
  lectors?: Lector[];

  @ManyToMany(() => Student, (student) => student.courses, {
    onDelete: 'CASCADE',
    onUpdate: 'NO ACTION',
  })
  students?: Student[];

  @OneToMany(() => Mark, (mark) => mark.course, { cascade: ['remove'] })
  marks?: Mark[];
}
