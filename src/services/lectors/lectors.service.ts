import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateLectorDto } from './dto/create-lector.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { Lector } from './entities/lector.entity';
import { CoursesService } from '../courses/courses.service';
import { GetCourseOfLectorDto } from './dto/get-course-of-lector.response.dto';
import { GetLectorDto } from './dto/get-lector.response.dto';
import { GetLectorWithCourseDto } from './dto/get-lector-with-course.response.dto';
import * as argon2 from 'argon2';
import { QueryFilterDto } from '../../application/dto/query.filter.dto';
import { UpdateLectorDto } from './dto/update-lector.dto';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorRepository: Repository<Lector>,
    private readonly coursesService: CoursesService,
  ) {}

  public async createLector(
    createLectorDto: CreateLectorDto,
  ): Promise<GetLectorDto> {
    try {
      const password = { ...createLectorDto };
      const hashPassword = await argon2.hash(password.password);
      createLectorDto.password = hashPassword;
      return await this.lectorRepository.save(createLectorDto);
    } catch (e) {
      throw new BadRequestException('Lector with this e-mail already exists');
    }
  }

  public async findAllLectors(
    queryFilter?: QueryFilterDto,
  ): Promise<GetLectorDto[]> {
    try {
      const where: Record<string, any> = {};

      if (queryFilter) {
        if (queryFilter.name) {
          where.name = ILike(`%${queryFilter.name}%`);
        }

        if (queryFilter.sortField && queryFilter.sortOrder) {
          const order = {
            [queryFilter.sortField]: queryFilter.sortOrder,
          };

          const lectors = await this.lectorRepository.find({
            where,
            order,
          });

          return lectors;
        }
      }

      const lectors = await this.lectorRepository.find({
        where,
      });

      return lectors;
    } catch (e) {
      throw e;
    }
  }

  public async findOneLector(id: string): Promise<GetLectorWithCourseDto> {
    try {
      const lector = await this.lectorRepository
        .createQueryBuilder('lector')
        .leftJoinAndSelect('lector.courses', 'course')
        .where('lector.id = :id', { id })
        .getOne();
      if (!lector) throw new NotFoundException('Lector not found');
      return lector;
    } catch (e) {
      throw e;
    }
  }

  public async findOneLectorByEmail(email: string): Promise<GetLectorDto> {
    try {
      const lector = await this.lectorRepository
        .createQueryBuilder('lector')
        .where('lector.email = :email', { email })
        .getOne();

      if (!lector)
        throw new NotFoundException('You entered an incorrect email');

      return lector;
    } catch (e) {
      throw e;
    }
  }

  public async updateLectorPassword(
    id: string,
    password: string,
  ): Promise<GetLectorDto> {
    try {
      const updateData = {
        password: await argon2.hash(password),
      };
      await this.lectorRepository.update(id, updateData);
      return await this.findOneLector(id);
    } catch (e) {
      throw e;
    }
  }

  public async updateLector(
    id: string,
    updateLectorDto: UpdateLectorDto,
  ): Promise<GetLectorDto> {
    try {
      if (updateLectorDto.password)
        updateLectorDto.password = await argon2.hash(updateLectorDto.password);
      await this.lectorRepository.update(id, updateLectorDto);
      return await this.findOneLector(id);
    } catch (e) {
      throw e;
    }
  }

  public async addCourseForLector(
    lectorId: string,
    courseId: string,
  ): Promise<GetLectorWithCourseDto> {
    try {
      const lector = await this.findOneLector(lectorId);
      const course = await this.coursesService.findOneCourse(courseId);

      const doubleCourse = await this.lectorRepository
        .createQueryBuilder('lector')
        .leftJoin('lector.courses', 'course')
        .where('lector.id = :lectorId', { lectorId })
        .andWhere('course.id = :courseId', { courseId })
        .getOne();

      if (doubleCourse)
        throw new BadRequestException(
          'The course has already been added to the lecturer',
        );

      lector.courses.push(course);
      return this.lectorRepository.save(lector);
    } catch (e) {
      throw e;
    }
  }

  public async getCourseOfLector(id: string): Promise<GetCourseOfLectorDto[]> {
    try {
      const lector = await this.lectorRepository
        .createQueryBuilder('lector')
        .leftJoin('lector.courses', 'course')
        .select([
          'lector.name AS "lectorName"',
          'course.id AS "courseId"',
          'course.name AS "courseName"',
        ])
        .where('lector.id = :id', { id })
        .getRawMany();
      if (!lector.length) {
        throw new NotFoundException('Lector not found');
      }
      return lector;
    } catch (e) {
      throw e;
    }
  }
}
