import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class GetCourseOfLectorDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Albus Dumbledore',
    type: String,
    description: 'The name of the lector',
  })
  lectorName: string;

  @IsUUID()
  @ApiProperty({
    example: '88087287-b2c0-4d39-ad5d-107cd9fc6cf3',
    type: String,
    description: 'UUID of the course',
  })
  courseId: string;

  @IsString()
  @ApiProperty({
    example: 'Magic',
    type: String,
    description: 'The name of the course',
  })
  courseName: string;
}
