import { ApiProperty } from '@nestjs/swagger';
import { GetCourseDto } from '../../courses/dto/get-course.response.dto';
import {
  IsArray,
  IsDate,
  IsEmail,
  IsNotEmpty,
  IsString,
  IsUUID,
} from 'class-validator';

export class GetLectorWithCourseDto {
  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: '45ee8032-16fe-414e-a164-37fd76997f0f',
    type: String,
    description: 'UUID of the lector',
  })
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Creation date and time of the lector',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Last update date and time of the lector',
  })
  updatedAt: Date;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Albus Dumbledore',
    type: String,
    description: 'The name of the lector',
  })
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'albusdumbledore@gmail.com',
    type: String,
    description: 'The email of the lector',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '123456qwe',
    type: String,
    description: 'The password of the lector',
  })
  password: string;

  @IsArray()
  @ApiProperty({
    example: {
      id: '88087287-b2c0-4d39-ad5d-107cd9fc6cf3',
      createdAt: '2023-08-26T15:03:34.773Z',
      updatedAt: '2023-08-26T15:03:34.773Z',
      name: 'Magic',
      description: 'Magic science',
      hours: 15,
    },
    type: GetCourseDto,
    description: 'Information about adding group',
  })
  courses?: GetCourseDto[];
}
