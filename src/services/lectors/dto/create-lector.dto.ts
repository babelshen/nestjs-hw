import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateLectorDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(3, {
    message: 'The name must contain a minimum of three characters',
  })
  @MaxLength(50, {
    message: 'The name should not be more than fifty characters',
  })
  @ApiProperty({
    example: 'Albus Dumbledore',
    type: String,
    description: 'The name of the lector',
  })
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @MinLength(5, {
    message: 'The email must contain a minimum of five characters',
  })
  @MaxLength(50, {
    message: 'The email should not be more than fifty characters',
  })
  @ApiProperty({
    example: 'albusdumbledore@gmail.com',
    type: String,
    description: 'The email of the lector',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6, {
    message: 'The password must contain a minimum of six characters',
  })
  @MaxLength(25, {
    message: 'The password should not be more than twenty five characters',
  })
  @ApiProperty({
    example: '123d345',
    type: String,
    description: 'The password of the lector',
  })
  password: string;
}
