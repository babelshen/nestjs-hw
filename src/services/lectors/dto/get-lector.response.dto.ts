import { ApiProperty } from '@nestjs/swagger';
import { IsDate, IsEmail, IsNotEmpty, IsString, IsUUID } from 'class-validator';

export class GetLectorDto {
  @IsNotEmpty()
  @IsUUID()
  @ApiProperty({
    example: '45ee8032-16fe-414e-a164-37fd76997f0f',
    type: String,
    description: 'UUID of the lector',
  })
  id: string;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Creation date and time of the lector',
  })
  createdAt: Date;

  @IsNotEmpty()
  @IsDate()
  @ApiProperty({
    example: '2023-08-27T10:28:57.019Z',
    type: Date,
    description: 'Last update date and time of the lector',
  })
  updatedAt: Date;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Albus Dumbledore',
    type: String,
    description: 'The name of the lector',
  })
  name: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'albusdumbledore@gmail.com',
    type: String,
    description: 'The email of the lector',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: '123456qwe',
    type: String,
    description: 'The password of the lector',
  })
  password: string;
}
