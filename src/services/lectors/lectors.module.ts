import { Module } from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';
import { CoursesModule } from '../courses/courses.module';

@Module({
  imports: [TypeOrmModule.forFeature([Lector]), CoursesModule],
  providers: [LectorsService],
  exports: [LectorsService],
})
export class LectorsModule {}
