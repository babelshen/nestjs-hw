import {
  Column,
  Entity,
  Index,
  JoinTable,
  ManyToMany,
  OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Mark } from '../..//marks/entities/mark.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'lectors' })
export class Lector extends CoreEntity {
  @Index()
  @ApiProperty({
    example: 'Albus Dumbledore',
    type: String,
    description: 'The name of the lector',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @ApiProperty({
    example: 'albusdumbledore@gmail.com',
    type: String,
    description: 'The email of the lector',
  })
  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  email: string;

  @ApiProperty({
    example: '123d45',
    type: String,
    description: 'The password of the lector',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  password: string;

  @ManyToMany(() => Course, (course) => course.lectors, {
    onDelete: 'CASCADE',
    onUpdate: 'NO ACTION',
  })
  @JoinTable({
    name: 'lector_course',
    joinColumn: {
      name: 'lector_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses?: Course[];

  @OneToMany(() => Mark, (mark) => mark.lector, { cascade: ['remove'] })
  marks: Mark[];
}
